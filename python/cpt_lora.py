#!/usr/bin/env python
# -*- coding: utf-8 -*-

#By P. Couderc
#Reception RS232/USB compteur énergie

import serial
import os
import time

global ser, ser1ora
ser = serial.Serial()
ser1ora = serial.Serial()

#PORT USB0 - Le premier connecté à la prise USB -------------------
ser.port = "/dev/ttyUSB0"
ser.baudrate=1200
ser.bytesize = serial.SEVENBITS     #number of bits per bytesfrom email.MIMEMultipart import MIMEMultipart
ser.parity = serial.PARITY_ODD     #set parity check
ser.stopbits = serial.STOPBITS_ONE  #number of stop bits
ser.timeout = 2     #timeout block read
                    #1. None: wait forever, block call
                    #2. 0: non-blocking mode, return immediately
                    #3. x, x is bigger than 0, float allowed, timeout block call
ser.xonxoff = False #disable software flow control
ser.rtscts = False  #disable hardware (RTS/CTS) flow control
ser.dsrdtr = False  #disable hardware (DSR/DTR) flow control
ser.writeTimeout = 2#timeout for write

if not ser.isOpen():    #on ouvre le port 0 si non ouvert *********
    ser.open()
    
#PORT USB1 - Le premier connecté à la prise USB -------------------
ser1ora.port = "/dev/ttyUSB1"
ser1ora.baudrate=57600
ser1ora.bytesize = serial.EIGHTBITS     #number of bits per bytesfrom email.MIMEMultipart import MIMEMultipart
ser1ora.parity = serial.PARITY_NONE     #set parity check
ser1ora.stopbits = serial.STOPBITS_ONE  #number of stop bits
ser1ora.timeout = 2     #timeout block read
                    #1. None: wait forever, block call
                    #2. 0: non-blocking mode, return immediately
                    #3. x, x is bigger than 0, float allowed, timeout block call
ser1ora.xonxoff = False #disable software flow control
ser1ora.rtscts = False  #disable hardware (RTS/CTS) flow control
ser1ora.dsrdtr = False  #disable hardware (DSR/DTR) flow control
ser1ora.writeTimeout = 2#timeout for write

if not ser1ora.isOpen():    #on ouvre le port 0 si non ouvert *********
    ser1ora.open()

ok=0
duree=0
tps_dep=time.time()-895         #calcul temps pour envoi dans 5 secondes
while True:
    rcv = ser.readline()        #lecture d'une chaine de type BYTES
    ch_rcv=rcv.decode("utf-8")  #conversion en chaine de type STR
    etiq=ch_rcv.split(' ')      #decoupage chaine (entre espace) en liste
    if (etiq[0]=='ADCO'):       #mémorisation numéro compteur si étiquette ADCO
        adco = etiq[1]          
        ok=ok+1
    if (etiq[0]=='BASE'):       #mémorisation numéro énergie si étiquette BASE
        base = etiq[1]
        base1 = etiq[1]
        nrj=float(int(base))
        base=str(float(nrj/1000))
        ok=ok+1
    duree=time.time() - tps_dep #calcul temps écoulé depuis dernier envoi
    if ((duree>900) and (ok>2)): #envoi Sigfox si données lues et durée écoulée
        print("La centrale n° ",adco," à produit kWh ",base)
        ok=0
        
        ser1ora.write(b'mac join otaa\r\n')  #join Lora
             
        time.sleep(10) # attente join 10s
        
        print("Accepted ?")
        ser1ora.write(b'mac tx cnf 1 ') ## transmission data
        ser1ora.write(adco.encode())
        ser1ora.write(base1.encode())
        ser1ora.write(b'\r\n')
           
        tps_dep=time.time()     #réinit temps de départ
 

    
