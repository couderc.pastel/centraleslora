#!/usr/bin/env python
# -*- coding: utf-8 -*-

#By P. Couderc
#Reception RS232/USB compteur énergie

import serial
import os
import time
# Import smtplib for the actual sending function
import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

global ser, ser1
ser = serial.Serial()
ser1 = serial.Serial()


#PORT USB0 - Le premier connecté à la prise USB -------------------
ser.port = "/dev/ttyUSB0"
ser.baudrate=     #A COMPLETER DEBIT
ser.bytesize = serial.SEVENBITS     #number of bits per bytesfrom email.MIMEMultipart import MIMEMultipart
ser.parity = serial.PARITY_ODD     #set parity check
ser.stopbits = serial.STOPBITS_ONE  #number of stop bits
ser.timeout = 2     #timeout block read
                    #1. None: wait forever, block call
                    #2. 0: non-blocking mode, return immediately
                    #3. x, x is bigger than 0, float allowed, timeout block call
ser.xonxoff = False #disable software flow control
ser.rtscts = False  #disable hardware (RTS/CTS) flow control
ser.dsrdtr = False  #disable hardware (DSR/DTR) flow control
ser.writeTimeout = 2#timeout for write

if not ser.isOpen():    #on ouvre le port 0 si non ouvert *********
    ser.open()

ok=0
duree=0
tps_dep=time.time()-1795         #calcul temps pour envoi dans 5 secondes
while True:
    rcv = ser.readline()        #lecture d'une chaine de type BYTES
    ch_rcv=rcv.decode("utf-8")  #conversion en chaine de type STR
    etiq=ch_rcv.split(' ')      #decoupage chaine (entre espace) en liste
    if (etiq[0]=='ADCO'):       #mémorisation numéro compteur si étiquette ADCO
        adco = etiq[1]          
        ok=ok+1
    if (etiq[0]=='BASE'):       #mémorisation numéro énergie si étiquette BASE
        base = etiq[1]
        nrj=float(int(base))
        base=str(float(nrj/1000))
        ok=ok+1
    duree=time.time() - tps_dep #calcul temps écoulé depuis dernier envoi
    if ((duree>1800) and (ok>2)): #envoi Sigfox si données lues et 11min30 (690s)
        print("La centrale n° ",adco," à produit kWh ",base)
        ok=0                    #réinit nombre étiquettes lues
        tps_dep=time.time()     #réinit temps de départ
        #Envoi du message par mail
        fromaddr = 'xxx@xxx.com' #mail expéditeur A COMPLETER
        toaddr = 'yyy@yyy.com'   #mail destinataire A COMPLETER
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "Relevé compteur centrale 1"
         
        body = "La centrale n° {} à produit {} kWh".format(adco,base)
        msg.attach(MIMEText(body, 'plain'))
        
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login('mail@gmail.com', 'password') #login et password
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        print("Mail envoye !")

    
