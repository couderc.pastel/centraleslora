    <?php
    // Afficher les erreurs à l'écran
    ini_set('display_errors', 1);
   // Afficher les erreurs et les avertissements
    error_reporting('e_all');
    ?><!doctype html>
<html>
	<head>
		<?php include 'php/models/head.php' ?>
		 <!-- Rafraichissement de la page toutes les 60s -->
		<meta http-equiv="Refresh" content="60">   
	</head>

	<body>
		<?php include 'php/models/header.php' ?>
		<?php include 'php/models/sidebar.php' ?>
		
		<div class="container">
			<div class="content">
				<div class="table-container">
					<table>
						<thead>
							<tr>
								<th class="thead-title">Date et heure</th>
								<th class="thead-title">Identifiant compteur</th>
								<th class="thead-title">Production cumulée (kWh)</th>
							</tr>
						</thead>

						<tbody>			
							<?php 
			//Ouverture du fichier de données******************************
								//lecture d'un fichier ligne par ligne
								$fic=fopen("compteurlora.csv", "r");
								$i=1 ;//Compteur de ligne
								while(!feof($fic))
									{
									$line= fgets($fic,1024);
									$param = explode (";",$line);
									//Enregistrement date et heure
									$timestamp =$param[1]/1000;
									
									// Le format
									$format = "d M Y à H:i:s";

									// Formatage en jour, mois, année, heure, minutes et secondes
									$date = date($format, $timestamp); // -> Saturday 01 Jan 2022 à 12:30:15
									if ($timestamp==0) $date ="";
									$adco = $param[3];
									$base = $param[2]/1000;
									if ($base==0) $base ="";
									?>
									<tr>
										<td><?= $date ?></td>
										<td><?= $adco ?></td>
										<td><?= $base ?></td>	
									</tr>
									<?php
									}
								fclose($fic) ; 
							?>

							    
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>